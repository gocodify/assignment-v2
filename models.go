package main

// Input ...
type Input struct {
	StartingTeam []Hiker  `yaml:"StartingTeam"`
	Bridges      []Bridge `yaml:"Bridges"`
}

// Bridge ...
type Bridge struct {
	Length           float64 `yaml:"Length"`
	AdditionalHikers []Hiker `yaml:"AdditionalHikers"`
	allHikers        []Hiker
}

// Hiker ...
type Hiker struct {
	Name  string  `yaml:"Name"`
	Speed float64 `yaml:"Speed"`
}
