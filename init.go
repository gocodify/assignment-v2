package main

import (
	"flag"
	"io/ioutil"
	"log"

	yaml "gopkg.in/yaml.v2"
)

var input Input

func init() {
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)

	flag.String("path", "", "Path to input.yaml")

	flag.Parse()

	path := flag.Lookup("path").Value.String()
	if path == "" {
		log.Fatal("-path is required. Path to input.yaml.")
	}

	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	if err := yaml.Unmarshal([]byte(fileBytes), &input); err != nil {
		log.Println("YAML file is not in a valid format.")
		log.Fatal(err)
	}
}
