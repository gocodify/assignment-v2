package main

import "log"

func case1() {
	log.Println("Showing outputs for 1st case")

	input.setAllHikersOnBridge(1)

	var totalTimeForAllCrossings float64

	for k, v := range input.Bridges {
		var timeToCrossEachBridge float64

		fastestHiker, indexOfFastestHiker := v.getFastestHiker()

		// Fastest Hiker will go on the bridge n - 1 times to drop fellow hikers where n is number of hikers.
		for m, n := range v.allHikers {
			if m == indexOfFastestHiker {
				continue
			}

			timeToCrossEachBridge += v.Length / n.Speed
		}

		// Fastest Hiker will return from the bridge n - 2 alone where n is number of hikers.
		timeToCrossEachBridge += ((float64(len(v.allHikers) - 2)) * v.Length) / fastestHiker.Speed

		totalTimeForAllCrossings += timeToCrossEachBridge

		log.Println("Time taken to cross bridge ", k+1, " = ", timeToCrossEachBridge, " minutes")
	}

	log.Println("Total time taken to cross all bridges = ", totalTimeForAllCrossings, " minutes")

	log.Println("1st case ends")
}

func case2() {
	log.Println("Showing outputs for 2nd case")

	input.setAllHikersOnBridge(2)

	var totalTimeForAllCrossings float64

	for k, v := range input.Bridges {
		var timeToCrossEachBridge float64

		fastestHiker, indexOfFastestHiker := v.getFastestHiker()

		// Fastest Hiker will go on the bridge n - 1 times to drop fellow hikers where n is number of hikers.
		for m, n := range v.allHikers {
			if m == indexOfFastestHiker {
				continue
			}

			timeToCrossEachBridge += v.Length / n.Speed
		}

		// Fastest Hiker will return from the bridge n - 2 alone where n is number of hikers.
		timeToCrossEachBridge += ((float64(len(v.allHikers) - 2)) * v.Length) / fastestHiker.Speed

		totalTimeForAllCrossings += timeToCrossEachBridge

		log.Println("Time taken to cross bridge ", k+1, " = ", timeToCrossEachBridge, " minutes")
	}

	log.Println("Total time taken to cross all bridges = ", totalTimeForAllCrossings, " minutes")

	log.Println("2nd case ends")
}
