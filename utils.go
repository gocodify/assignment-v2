package main

func (input *Input) setAllHikersOnBridge(solutionCase int) {
	for k, v := range input.Bridges {
		hiker := make([]Hiker, 0)
		input.Bridges[k].allHikers = hiker

		if k == 0 || solutionCase == 2 {
			input.Bridges[k].allHikers = append(input.Bridges[k].allHikers, input.StartingTeam...)
			input.Bridges[k].allHikers = append(input.Bridges[k].allHikers, v.AdditionalHikers...)

			continue
		}

		input.Bridges[k].allHikers = append(input.Bridges[k].allHikers, input.Bridges[k-1].allHikers...)
		input.Bridges[k].allHikers = append(input.Bridges[k].allHikers, v.AdditionalHikers...)
	}
}

func (bridge *Bridge) getFastestHiker() (Hiker, int) {
	var fastestHiker Hiker
	var indexOfFastestHiker int

	for k, v := range bridge.allHikers {
		if v.Speed > fastestHiker.Speed {
			fastestHiker = v
			indexOfFastestHiker = k
		}
	}

	return fastestHiker, indexOfFastestHiker
}
