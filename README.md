# Usage #

Install go as specified on https://golang.org. Then follow the following steps:
- Clone this repository in $HOME/go/src
- cd assignment-v2

To run this project:
- go install && assignment-v2 -path input.yaml 
- The program takes a command line argument -path, which is path to input yaml file.

# Solution Approach #

To find the minimum time that the hikers take to cross a bridge, I followed the following approach.

- A team of n hikers starts hiking.
- At each bridge, we find the total number of hikers that are about to cross the bridge, which is the sum of starting team hikers and additional hikers which are encountered on their way.
- Find the fastest hiker (whose speed is highest among all the hikers who are about to cross the bridge).
- Fastest hiker will drop their fellow hikers one by one to the other end of the bridge.
- This is due to the fact that, only 2 hikers can cross the bridge at the same time and since they have only one torch so one hiker will have to return alone with the torch. To minimize the return time, I found the fastest hiker so that he can return in the minimum possible time.
- So, if there are x number of hikers who have to cross a bridge (starting hikers + additional hikers) then the fastest hiker will have do the return trip x-2 times alone with his own speed. While droping the fellow hikers, he will do x-1 forward trips at the speed of the fellow hikers.

# Solutions #

While going through the problem statement, I found out that there are two different cases through which we can approach the solution. In both the cases, basic approach (mentioned above) will remain same.

* **First case** - Additional hiker(s) encountered on each bridge will cross further bridges with the team. For eg: If initial team size is 4. On fisrt bridge, they encountered 1 additional hiker so total 5 hikers will cross the bridge. On second bridge they encountered 3 additional hikers so total 8 hikers will cross the bridge.

* **Second case** - Additiona hiker(s) encountered on each bridge only needs help to cross that particular bridge. They will not be a part of initial team going further.

## Input ##

```yaml
---
StartingTeam:
- Name: A
  Speed: 100
- Name: B
  Speed: 50
- Name: C
  Speed: 20
- Name: D
  Speed: 10
Bridges:
- Length: 100
  AdditionalHikers: []
- Length: 250
  AdditionalHikers:
  - Name: E
    Speed: 2.5
- Length: 150
  AdditionalHikers:
  - Name: F
    Speed: 25
  - Name: G
    Speed: 15
```

## Output ##

![picture alt](https://gitlab.com/gocodify/assignment-v2/raw/master/output.png)

## Output Explanation ##

The above image shows outputs for both the cases. 

### Case 1 ###

As mentioned above, all the additional hikers will be part of the team while covering the upcomming bridges.

**In the input there are no additional hikers on bridge 1 so A, B, C and D will cross the bridge. Length of bridge is 100 ft.**
- Since A has the highest speed so he will cross the bridge with fellow hikers and then return.
- Total time taken to drop B = (100 / 50) + (100 / 100) = 3 minutes
- Total time taken to drop C = (100 / 20) + (100 / 100) = 6 minutes
- Total time taken to drop D = (100 / 10) = 10 minutes
- Total time taken to cross Bridge 1 = 19 minutes

**Now there is a additional hiker E on bridge 2.**
- Again A has the highest speed so he will cross the bridge with fellow hikers and then return. Length of bridge is 250 ft.
- Total time taken to drop B = (250 / 50) + (250 / 100) = 7.5 minutes
- Total time taken to drop C = (250 / 20) + (250 / 100) = 15 minutes
- Total time taken to drop D = (250 / 10) + (250 / 100)  = 27.5 minutes
- Total time taken to drop E = (250 / 2.5) = 100 minutes
- Total time taken to cross Bridge 2 = 150 minutes

**Now there are two additional hikers F and G are on bridge 3.**
- Again A has the highest speed so he will cross the bridge with fellow hikers and then return. Length of bridge is 150 ft.
- Total time taken to drop B = (150 / 50) + (150 / 100) = 4.5 minutes
- Total time taken to drop C = (150 / 20) + (150 / 100) = 9 minutes
- Total time taken to drop D = (150 / 10) + (150 / 100)  = 16.5 minutes
- Total time taken to drop E = (150 / 2.5) + (150 / 100) = 61.5 minutes
- Total time taken to drop F = (150 / 25) + (150 / 100) = 7.5 minutes
- Total time taken to drop G = (150 / 15) = 10 minutes
- Total time taken to cross Bridge 3 = 109 minutes

**Total time taken to cross all the bridges = 278 minutes**

### Case 2 ###

Additional hikers will not be part of the team while covering the next bridges.

**In the input there are no additional hikers on bridge 1 so A, B and C will cross the bridge. Length of bridge is 100 ft.**
- Since A has the highest speed so he will cross the bridge with fellow hikers and then return.
- Total time taken to drop B = (100 / 50) + (100 / 100) = 3 minutes
- Total time taken to drop C = (100 / 20) + (100 / 100) = 6 minutes
- Total time taken to drop D = (100 / 10) = 10 minutes
- Total time taken to cross Bridge 1 = 19 minutes

**Now there is a additional hiker E on bridge 2 ft.**
- Again A has the highest speed so he will cross the bridge with fellow hikers and then return. Length of bridge is 250 ft.
- Total time taken to drop B = (250 / 50) + (250 / 100) = 7.5 minutes
- Total time taken to drop C = (250 / 20) + (250 / 100) = 15 minutes
- Total time taken to drop D = (250 / 10) + (250 / 100)  = 27.5 minutes
- Total time taken to drop E = (250 / 2.5) = 100 minutes
- Total time taken to cross Bridge 2 = 150 minutes

**Now there are two additional hikers F and G are on bridge 3 ft.**
- Again A has the highest speed so he will cross the bridge with fellow hikers and then return. Length of bridge is 150 ft.
- Total time taken to drop B = (150 / 50) + (150 / 100) = 4.5 minutes
- Total time taken to drop C = (150 / 20) + (150 / 100) = 9 minutes
- Total time taken to drop D = (150 / 10) + (150 / 100)  = 16.5 minutes
- Total time taken to drop F = (150 / 25) + (150 / 100) = 7.5 minutes
- Total time taken to drop G = (150 / 15) = 10 minutes
- Total time taken to cross Bridge 3 = 47.5 minutes

**Total time taken to cross all the bridges = 216.5 minutes**
